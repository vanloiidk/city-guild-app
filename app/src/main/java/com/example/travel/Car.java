package com.example.travel;

public class Car {
    private int available;
    private String description;
    private int duration;
    private String image;
    private int likes;
    private String name;
    private int price;
    private int rent;

    public Car() {
    }

    public Car(int available, String description, int duration, String image, int likes, String name, int price, int rent) {
        this.available = available;
        this.description = description;
        this.duration = duration;
        this.image = image;
        this.likes = likes;
        this.name = name;
        this.price = price;
        this.rent = rent;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }
}
