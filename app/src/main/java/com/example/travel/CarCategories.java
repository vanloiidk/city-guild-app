package com.example.travel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class CarCategories extends AppCompatActivity {
    private RecyclerView myCarList;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_categories);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("CarList");
        mDatabase.keepSynced(true);

        myCarList = (RecyclerView) findViewById(R.id.myrecycleview);
        myCarList.setHasFixedSize(true);
        myCarList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Car, CarViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Car, CarViewHolder>
                (Car.class, R.layout.activity_car_card, CarViewHolder.class, mDatabase) {
            @Override
            protected void populateViewHolder(CarViewHolder carViewHolder, final Car car, int i) {
                carViewHolder.setName(car.getName());
                carViewHolder.setAvailable((int) car.getAvailable());
                carViewHolder.setPrice(car.getPrice());
                carViewHolder.setDuration(car.getDuration());
                carViewHolder.setLikes(car.getLikes());
                carViewHolder.setRent(car.getRent());
                carViewHolder.setImage(getApplicationContext(), car.getImage());
                carViewHolder.linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CarCategories.this, CarDetail.class);
                        intent.putExtra("name", car.getName());
                        intent.putExtra("description", car.getDescription());
                        intent.putExtra("price", car.getPrice());
                        intent.putExtra("image", car.getImage());
                        startActivity(intent);
                    }
                });
            }
        };

        myCarList.setAdapter(firebaseRecyclerAdapter);
    }

    public static class CarViewHolder extends RecyclerView.ViewHolder {
        View mView;
        LinearLayout linear;
        public CarViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            linear = itemView.findViewById(R.id.linear);
        }

        public void setName(String name) {
            TextView car_name = (TextView) mView.findViewById(R.id.car_name);
            car_name.setText(name);

        }

        public void setAvailable(int available) {
            TextView car_available = (TextView) mView.findViewById(R.id.car_available);
            car_available.setText(String.valueOf(available));
        }

        public void setPrice(int price) {
            TextView car_price = (TextView) mView.findViewById(R.id.car_price);
            car_price.setText(String.valueOf(price));
        }

        public void setDuration(int duration) {
            TextView car_duration = (TextView) mView.findViewById(R.id.car_duration);
            car_duration.setText(String.valueOf(duration));
        }

        public void setLikes(int likes) {
            TextView car_likes = (TextView) mView.findViewById(R.id.car_likes);
            car_likes.setText(String.valueOf(likes));
        }

        public void setRent(int rent) {
            TextView car_rent = (TextView) mView.findViewById(R.id.car_rent);
            car_rent.setText(String.valueOf(rent));
        }

        public void setImage(Context ctx, String image) {
            ImageView car_image = (ImageView) mView.findViewById(R.id.car_image);
            Picasso.with(ctx).load(image).into(car_image);
        }
    }
}
