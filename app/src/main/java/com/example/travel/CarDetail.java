package com.example.travel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class CarDetail extends AppCompatActivity {
    ImageView car_image;
    TextView car_name;
    TextView car_price;
    TextView car_description;
    String name, description, image;
    int price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_detail);

        car_image = findViewById(R.id.car_image);
        car_name = findViewById(R.id.car_name);
        car_price = findViewById(R.id.car_price);
        car_description = findViewById(R.id.car_description);

        name = getIntent().getStringExtra("name");
        description = getIntent().getStringExtra("description");
        image = getIntent().getStringExtra("image");
        price = getIntent().getIntExtra("price", 0);
        car_name.setText(name);
        car_description.setText(description);
        car_price.setText(String.valueOf(price));

        Picasso.with(getApplicationContext()).load(image).into(car_image);
    }
}
