package com.example.travel.User;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.example.travel.CarCategories;
import com.example.travel.R;

public class AllCategories extends AppCompatActivity {

    ImageView backBt;
    Button carExpandBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_all_categories);

        // Hooks
        backBt = findViewById(R.id.category_back_pressed);
        backBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllCategories.super.onBackPressed();
            }
        });

        carExpandBtn = findViewById(R.id.car_expand_btn);

        carExpandBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCarCategoriesActivity();
            }
        });
    }

    public void openCarCategoriesActivity() {
        Intent intent = new Intent(AllCategories.this, CarCategories.class);
        startActivity(intent);
    }
}
