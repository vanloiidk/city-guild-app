package com.example.travel.User;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.travel.Common.LoginSignup.RetailerStartUpScreen;
import com.example.travel.HelperClasses.HomeAdapter.CategoriesViewAdapter;
import com.example.travel.HelperClasses.HomeAdapter.CategoriesViewHelperClass;
import com.example.travel.HelperClasses.HomeAdapter.FeaturedAdapter;
import com.example.travel.HelperClasses.HomeAdapter.FeaturedHelperClass;
import com.example.travel.HelperClasses.HomeAdapter.MostViewAdapter;
import com.example.travel.HelperClasses.HomeAdapter.MostViewHelperClass;
import com.example.travel.R;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class UserDashboard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    static final float END_SCALE = 0.7f;
    RecyclerView featuredRecycler ;
    RecyclerView mostViewRecycler;
    RecyclerView categoryRecycler;

    RecyclerView.Adapter adapter;
    ImageView menuIcon;

    // Drawer name
    DrawerLayout drawerLayout;
    NavigationView navigationView;

    LinearLayout contentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_dashboard);

        // Hooks
        contentView = findViewById(R.id.content);
        featuredRecycler =findViewById(R.id.featured_recycler);
        featuredRecycler();

        mostViewRecycler = findViewById(R.id.mostView_recycler);
        mostViewRecycler();

        categoryRecycler = findViewById(R.id.category_recycler);
        categoryRecycler();

        menuIcon = findViewById(R.id.menu_icon);

        // Menu hooks

        drawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.navigation_view);




        navigationDrawer();
    }

    // Navigation Drawer Function
    private void navigationDrawer() {
        // Navigation Drawer

        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(drawerLayout.isDrawerVisible(GravityCompat.START)){
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else{
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });


        animateNavigationDrawer();
    }

    private void animateNavigationDrawer() {

        drawerLayout.setScrimColor(getResources().getColor(R.color.colorPrimary));

        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                final float offsetScale = 1- diffScaledOffset;
                contentView.setScaleX(offsetScale);
                contentView.setScaleY(offsetScale);

                final float xOffset = drawerView.getWidth()*slideOffset;
                final float xOffsetDiff = contentView.getWidth() * diffScaledOffset / 2 ;
                final  float xTranslation = xOffset - xOffsetDiff;
                contentView.setTranslationX(xTranslation);


            }

        });
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerVisible(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.nav_all_categories:
                Intent intent = new Intent(getApplicationContext(), AllCategories.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    private void categoryRecycler() {
        categoryRecycler.setHasFixedSize(true);
        categoryRecycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));

        ArrayList<CategoriesViewHelperClass> categories = new ArrayList<>();

        categories.add(new CategoriesViewHelperClass(R.drawable.mc_donal,"Restaurant"));
        categories.add(new CategoriesViewHelperClass(R.drawable.city_1,"Hotel"));
        categories.add(new CategoriesViewHelperClass(R.drawable.city_2,"Shop"));
        categories.add(new CategoriesViewHelperClass(R.drawable.city_1,"Airport"));

        adapter = new CategoriesViewAdapter(categories);
        categoryRecycler.setAdapter(adapter);
    }

    private void mostViewRecycler() {
        mostViewRecycler.setHasFixedSize(true);
        mostViewRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        ArrayList<MostViewHelperClass> mostViewItems = new ArrayList<>();

        mostViewItems.add(new MostViewHelperClass(R.drawable.mc_donal, "McDonald's", "We serve awesome food and drind that you can take every where"));
        mostViewItems.add(new MostViewHelperClass(R.drawable.city_1, "Edenrobe", "We serve awesome food and drind that you can take every where"));
        mostViewItems.add(new MostViewHelperClass(R.drawable.city_2, "Sweet and Bakers", "We serve awesome food and drind that you can take every where"));

        adapter = new MostViewAdapter(mostViewItems);
        mostViewRecycler.setAdapter(adapter);
    }

    private void featuredRecycler(){
        featuredRecycler.setHasFixedSize(true);
        featuredRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));

        ArrayList<FeaturedHelperClass> featureLocations = new ArrayList<>();

        featureLocations.add(new FeaturedHelperClass(R.drawable.mc_donal,"McDonald's","We serve awesome food and drind that you can take every where"));
        featureLocations.add(new FeaturedHelperClass(R.drawable.city_1,"Edenrobe","We serve awesome food and drind that you can take every where"));
        featureLocations.add(new FeaturedHelperClass(R.drawable.city_2,"Sweet and Bakers","We serve awesome food and drind that you can take every where"));
        adapter = new FeaturedAdapter(featureLocations);
        featuredRecycler.setAdapter(adapter);

    }


    public void callRetailerScreens(View view){
        startActivity(new Intent(getApplicationContext(), RetailerStartUpScreen.class));
    }


}
