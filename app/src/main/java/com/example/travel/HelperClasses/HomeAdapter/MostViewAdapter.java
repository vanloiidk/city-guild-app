package com.example.travel.HelperClasses.HomeAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.travel.R;

import java.util.List;

public class MostViewAdapter extends RecyclerView.Adapter<MostViewAdapter.MSViewHolder> {
    List<MostViewHelperClass> mostViewItems;

    public MostViewAdapter(List<MostViewHelperClass> mostViewItems) {
        this.mostViewItems = mostViewItems;
    }

    @NonNull
    @Override
    public MSViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.most_viewed_card_design, parent,false);

        return new MSViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MSViewHolder holder, int position) {
        MostViewHelperClass MVItem = mostViewItems.get(position);

        holder.imageView.setImageResource(MVItem.getImage());
        holder.title.setText(MVItem.getTitle());
        holder.desc.setText(MVItem.getDesc());
    }

    @Override
    public int getItemCount() {
        return mostViewItems.size();
    }


    public static class MSViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView title;
        TextView desc;

        public MSViewHolder(@NonNull View itemView) {
            super(itemView);

            // Hooks
            imageView = itemView.findViewById(R.id.mv_image);
            title = itemView.findViewById(R.id.mv_title);
            desc = itemView.findViewById(R.id.mv_desc);
        }
    }
}
