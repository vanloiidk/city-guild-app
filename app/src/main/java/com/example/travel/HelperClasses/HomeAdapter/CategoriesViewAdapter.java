package com.example.travel.HelperClasses.HomeAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.travel.R;


import java.util.List;

public class CategoriesViewAdapter extends RecyclerView.Adapter<CategoriesViewAdapter.CategoriesViewHolder> {

    List<CategoriesViewHelperClass> categories;

    public CategoriesViewAdapter(List<CategoriesViewHelperClass> categories) {
        this.categories = categories;
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_card_design, parent, false);

        return new CategoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesViewHolder holder, int position) {
        CategoriesViewHelperClass category = categories.get(position);

        holder.imageView.setImageResource(category.getImage());
        holder.nameView.setText(category.getName());
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }


    public static class CategoriesViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView nameView;


        public CategoriesViewHolder(@NonNull View itemView) {
            super(itemView);

            // Hooks
            imageView = itemView.findViewById(R.id.category_image);
            nameView = itemView.findViewById(R.id.category_name);
        }
    }
}
