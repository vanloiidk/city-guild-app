package com.example.travel.HelperClasses.HomeAdapter;

public class CategoriesViewHelperClass {
    int image;
    String name;

    public CategoriesViewHelperClass(int image, String name) {
        this.image = image;
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public String getName() {
        return name;
    }
}
